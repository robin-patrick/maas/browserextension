import * as $ from "jquery";

function save_options() {
  const username = $("#username").val();
  const password = $("#password").val();
  const development = $("#development").is(":checked");
  chrome.storage.sync.set(
    {
      username,
      password,
      development,
    },
    function () {
      console.log(development);
    }
  );
}

$("#save").click(save_options);
