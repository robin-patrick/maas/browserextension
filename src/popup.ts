import * as $ from "jquery";

class LoginResponse {
  access_token: string;
}
class MemeResponse {
  status: string;
}

function addMeme(url: string, callback: any) {
  chrome.storage.sync.get(
    {
      username: "",
      password: "",
      development: false,
    },
    function (items: {
      username: string;
      password: string;
      development: boolean;
    }) {
      let baseUrl = items.development
        ? "https://maas-api.k8s.domnick.io/"
        : "https://api.mymemelist.com/";
      console.log(baseUrl);
      $.ajax({
        type: "POST",
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        url: baseUrl + "auth/login",
        data: JSON.stringify({
          username: items.username,
          password: items.password,
        }),
        error: function (err: JQuery.jqXHR<any>) {
          callback("Login Failed: " + err.statusText);
        },
        success: function (login: LoginResponse) {
          $.ajax({
            type: "POST",
            dataType: "JSON",
            contentType: "application/json; charset=utf-8",
            url: baseUrl + "api/meme",
            data: JSON.stringify({
              url,
            }),
            headers: {
              Authorization: "Bearer " + login.access_token,
            },
            error: function (err: JQuery.jqXHR<any>) {
              callback("Meme could not be added: " + err.statusText);
            },
            success: function (meme: MemeResponse) {
              callback(meme.status);
            },
          });
        },
      });
    }
  );
}

$(function () {
  const queryInfo = {
    active: true,
    currentWindow: true,
  };
  const urlField = $("#url");

  chrome.tabs.query(queryInfo, function (tabs) {
    const url = tabs[0].url;
    console.log(tabs);
    urlField.html("Adding: " + url);
    addMeme(url, function (result: string) {
      urlField.html(result);
    });
  });
});

function setUpContextMenus() {
  chrome.contextMenus.create({
    title: "Add this Meme",
    id: "AddMeme",
    contexts: ["link"],
  });
}

chrome.runtime.onInstalled.addListener(function () {
  setUpContextMenus();
});

chrome.contextMenus.onClicked.addListener(function (itemData) {
  addMeme(itemData.linkUrl, function (result: string) {
    console.log(result);
  });
});